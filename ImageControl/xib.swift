import UIKit

class CustomAlert: UIView {


    
    class func instanceFromNib() -> CustomAlert {
        guard let customAlert = UINib(nibName: "CustomAlert", bundle: nil).instantiate(withOwner: nil, options: nil).first as? CustomAlert else {
            return CustomAlert()
        }
        return customAlert
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()

    }
   
    @IBAction func okPressed(_ sender: UIButton) {
        self.removeFromSuperview()
    }
}
