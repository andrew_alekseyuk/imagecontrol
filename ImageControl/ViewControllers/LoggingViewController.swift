import UIKit
import AudioToolbox
import SwiftyKeychainKit

class LoggingViewController: UIViewController {
    @IBOutlet weak var applicationName: UILabel!
    @IBOutlet weak var passwordForm: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var errorMessage: UILabel!
    
    @IBOutlet weak var passwordContainer: UIView!
    
    
    var incorrectPasswordCount = 4
    
    let gradient = CAGradientLayer()
    let pulse = CASpringAnimation(keyPath: "transform.scale")
 
    override func viewDidLoad() {
        super.viewDidLoad()
        var password:String?

        
        do {
            password = try Manager.shared.keychain.get(Manager.shared.accessTokenKeyOne)
        } catch {
            print ("Passwords are not set")
        }
        if password == nil {
            let SetPasswordViewController = self.storyboard?.instantiateViewController(identifier: "SetPasswordsViewController") as! SetPasswordsViewController
            self.navigationController?.pushViewController(SetPasswordViewController, animated: true)
        }

        self.view.becomeFirstResponder()
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
        
        errorMessage.text = ""
        passwordForm.text = ""
        passwordContainer.layer.cornerRadius = passwordContainer.frame.height / 2
        
        
        applicationName.font = UIFont(name: "Outlier", size: 55)
        applicationName.textColor = hexStringToUIColor(hex: "cdd806")
        applicationName.textAlignment = .center
        applicationName.layer.cornerRadius = applicationName.frame.height / 2
        
        loginButton.layer.cornerRadius = loginButton.frame.height / 2
        addGradient(button: loginButton)
        loginButton.clipsToBounds = true

    }
    
    @IBAction func myButton(_ sender: UIButton) {

        let firstPassword = self.storyboard?.instantiateViewController(withIdentifier: "FirstPasswordViewController") as! FirstPasswordViewController
        self.navigationController?.pushViewController(firstPassword, animated: true)
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func addGradient(button: UIButton) {
                
 
                gradient.colors = [hexStringToUIColor(hex:"cdd806").cgColor,hexStringToUIColor(hex:"fc9d94").cgColor,hexStringToUIColor(hex:"cdd806").cgColor]
        
                gradient.opacity = 1
                gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
                gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
                
                gradient.frame = button.bounds
                gradient.name = "gradient"
                button.layer.insertSublayer(gradient, at: 0)
            }
    
  
    func  checkPassword () {
        var firstPassword:String?
        var secondPassword: String?
    
        do {
            firstPassword = try Manager.shared.keychain.get(Manager.shared.accessTokenKeyOne)
            secondPassword = try Manager.shared.keychain.get(Manager.shared.accessTokenKeyTwo)
        } catch {
            print ("Passwords are not set")
        }
        
        if passwordForm.text == firstPassword {
            incorrectPasswordCount = 4
            errorMessage.text = ""
            passwordForm.text = ""
            let firstPassword = self.storyboard?.instantiateViewController(withIdentifier: "FirstPasswordViewController") as! FirstPasswordViewController
                self.navigationController?.pushViewController(firstPassword, animated: true)
        } else if passwordForm.text == secondPassword {
            incorrectPasswordCount = 4
            errorMessage.text = ""
                passwordForm.text = ""
                let MySecondPasswordViewController = self.storyboard?.instantiateViewController(withIdentifier: "SecondPasswordViewController") as! SecondPasswordViewController
                self.navigationController?.pushViewController(MySecondPasswordViewController, animated: true)
            
        } else {
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        incorrectPasswordCount -= 1
            if incorrectPasswordCount > 0 {
            errorMessage.text = "Password is incorrect. Data will be dropped in \(incorrectPasswordCount) unsuccessfull tries"
        errorMessage.textColor = .red
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: errorMessage.center.x - 10, y: errorMessage.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: errorMessage.center.x + 10, y: errorMessage.center.y))
        errorMessage.layer.add(animation, forKey: "position")
            } else {
                errorMessage.textColor = .red
                errorMessage.text = "Error. Prifile doesn't exist. Re-launch the App to register once again"
                // remove keys
                try! Manager.shared.keychain.remove(Manager.shared.accessTokenKeyOne)
                try! Manager.shared.keychain.remove(Manager.shared.accessTokenKeyTwo)
                //drop data
                let emptyArray:[Picture] = []
                UserDefaults.standard.set(emptyArray, forKey: Keys.imageSet.rawValue)
            }
    }
}
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        
        //why doesn't work with correct passwords?????
        sender.pulsate()
        checkPassword()
    }
    
}

extension LoggingViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        passwordForm.resignFirstResponder()
        
        return true
    }
}

