import UIKit


class ViewerViewController: UIViewController, FirstPasswordViewControllerDelegate {

    @IBOutlet weak var topContainer: UIView!
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var commentContainer: UIView!
    @IBOutlet weak var buttonsContainer: UIView!
    
    @IBOutlet weak var imageViewOne: UIImageView!
    @IBOutlet weak var imageViewTwo: UIImageView!
    @IBOutlet weak var imageViewThree: UIImageView!
    
    @IBOutlet weak var imageLeftHalf: UIView!
    @IBOutlet weak var imageRightHalf: UIView!
    
    
    @IBOutlet weak var commentSection: UITextField!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var blur: UIVisualEffectView!
    
    
    @IBOutlet weak var imageViewOneLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewTwoLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageViewThreeLeftConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var topContainerTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonsContainerBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imageContainerBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var iamgeContainerTopConstraint: NSLayoutConstraint!
    
    
    // MARK: - Lets
    
    // MARK: - Vars
    var indexImg = 0
    var fullScreenModeStatus = false
    var imageIndexForSaving = 0
    var loadedArrayOfImages:[UIImage] = []
    var firstPasswordVC = FirstPasswordViewController?.self
    var imageArray = UserDefaults.standard.value([Picture].self, forKey: Keys.imageSet.rawValue)!
    
   
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewController = segue.destination as? FirstPasswordViewController {
            viewController.delegate = self
        }
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.navigationController?.interactivePopGestureRecognizer!.delegate = self
        loadAllImages()
        registerForKeyboardNotifications()
        self.blur.isHidden = true
        
        let swipeLeftOnLeftHalf = UISwipeGestureRecognizer(target: self, action: #selector(swipeLeftAction))
        swipeLeftOnLeftHalf.direction = .left
        self.imageLeftHalf.addGestureRecognizer(swipeLeftOnLeftHalf)
        
        let swipeLeftOnRightHalf = UISwipeGestureRecognizer(target: self, action: #selector(swipeRightAction))
        swipeLeftOnRightHalf.direction = .left
        self.imageRightHalf.addGestureRecognizer(swipeLeftOnRightHalf)
        
        
        let tap = UITapGestureRecognizer (target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tap)
        
        let tapTwo = UITapGestureRecognizer (target: self, action: #selector(fullScreenMode))
        self.imageLeftHalf.addGestureRecognizer(tapTwo)
        
        let tapThree = UITapGestureRecognizer (target: self, action: #selector(fullScreenMode))
        self.imageRightHalf.addGestureRecognizer(tapThree)
        
        let swipeOnEdge = preferredScreenEdgesDeferringSystemGestures
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        drawViewThree()
        checkLikeStatus()
        loadComment()
    }
    
    //MARK: - Lifecycle functions
    func drawViewOne () {
        if indexImg < 0 {
            indexImg = imageArray.count - 1
        } else if indexImg > imageArray.count - 1 {
            indexImg = 0
        }
        
        imageViewOneLeftConstraint.constant = 0
        self.view.layoutIfNeeded()
        imageViewOne.image = loadedArrayOfImages[indexImg]
        imageViewOne.contentMode = .scaleAspectFit
        self.imageContainer.addSubview(imageViewOne)
        self.imageContainer.bringSubviewToFront(imageLeftHalf)
        self.imageContainer.bringSubviewToFront(imageRightHalf)
        self.imageContainer.addSubview(imageLeftHalf)
        self.imageContainer.addSubview(imageRightHalf)
    }
    
    func drawViewTwo()  {
        imageViewTwo.isHidden = false
        if indexImg < 0 {
            indexImg = imageArray.count - 1
        } else if indexImg > imageArray.count - 1 {
            indexImg = 0
        }
        imageViewTwoLeftConstraint.constant = 1000
        self.view.layoutIfNeeded()
        imageViewTwo.image = loadedArrayOfImages[indexImg]
        imageViewTwo.contentMode = .scaleAspectFit
        self.imageContainer.addSubview(imageViewTwo)
        self.imageContainer.bringSubviewToFront(imageLeftHalf)
        self.imageContainer.bringSubviewToFront(imageRightHalf)
        self.imageContainer.addSubview(imageLeftHalf)
        self.imageContainer.addSubview(imageRightHalf)
        
    }
    
    func drawViewThree () {
        imageViewThree.isHidden = false
        if indexImg < 0 {
            indexImg = imageArray.count - 1
        } else if indexImg > imageArray.count - 1 {
            indexImg = 0
        }
        imageViewThreeLeftConstraint.constant = 0
        self.view.layoutIfNeeded()
        imageViewThree.image = loadedArrayOfImages[indexImg]
        imageViewThree.contentMode = .scaleAspectFit
        self.imageContainer.addSubview(imageViewThree)
        self.imageContainer.bringSubviewToFront(imageLeftHalf)
        self.imageContainer.bringSubviewToFront(imageRightHalf)
    }
    
    func imageGoesBack () {
        imageArray[indexImg].like = likeButton.isSelected
        imageArray[indexImg].comment = commentSection.text
        UserDefaults.standard.set(encodable: imageArray, forKey: Keys.imageSet.rawValue)
        indexImg -= 1
        checkLikeStatus()
        loadComment()
        drawViewThree()
        indexImg += 1
        drawViewOne()
        imageViewOneLeftConstraint.constant -= 1000
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
        if indexImg == 0 {
            indexImg = imageArray.count - 1
        } else {
            indexImg -= 1
        }
        imageViewTwo.isHidden = true
        
    }
    
    func imageGoesForward () {
        imageArray[indexImg].like = likeButton.isSelected
        imageArray[indexImg].comment = commentSection.text
        UserDefaults.standard.set(encodable: imageArray, forKey: Keys.imageSet.rawValue)
        drawViewThree()
        indexImg += 1
        checkLikeStatus()
        loadComment()
        drawViewTwo()
        imageViewTwoLeftConstraint.constant -= 1000
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        } completion: { (done) in
            self.imageViewThree.isHidden = true
        }
    }
    
    func checkLikeStatus () {
        
        if indexImg < 0 {
            indexImg = imageArray.count - 1
        } else if indexImg > imageArray.count - 1 {
            indexImg = 0
        }
        likeButton.isSelected = imageArray[indexImg].like!
    }
    
    func loadComment () {
        commentSection.text = imageArray[indexImg].comment
    }
    
    func setImageIndex (index: Int) {
        self.indexImg = index
    }
    
    func loadAllImages () {
        
        if imageIndexForSaving > imageArray.count - 1 {
            return
        } else {
            guard let loadedImage = Manager.shared.loadImage(fileName: imageArray[imageIndexForSaving].imageURL!) else {return}
            self.loadedArrayOfImages.append(loadedImage)
            imageIndexForSaving += 1
            loadAllImages()
        }
    }
    
    func getImageIndex(index: Int) {
        self.indexImg = index
        print("recieved index is \(index)")
        print("Not recieved index")
    }
    
    private func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @IBAction private func keyboardWillShow(_ notification: NSNotification) {
        
        guard let userInfo = notification.userInfo,
              let animationDuration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue,
              let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            topContainerTopConstraint.constant = 0
            buttonsContainerBottomConstraint.constant = 0
        } else {
            topContainerTopConstraint.constant = -keyboardScreenEndFrame.height
            buttonsContainerBottomConstraint.constant = keyboardScreenEndFrame.height
        }
        view.needsUpdateConstraints()
        UIView.animate(withDuration: animationDuration) {
            self.view.layoutIfNeeded()
        }
    }

    // MARK: - IBActions
    @objc func hideKeyboard() {
        UserDefaults.standard.set(encodable: imageArray, forKey: Keys.imageSet.rawValue)
        view.endEditing(true)
    }
    @objc func fullScreenMode () {
        
        if commentSection.isEditing {
            view.endEditing(true)
        } else {
            
            if fullScreenModeStatus == false {
                view.endEditing(true)
                self.blur.isHidden = false
                self.view.addSubview(blur)
                self.view.addSubview(imageContainer)
                
                
                self.iamgeContainerTopConstraint.constant = -self.topContainer.frame.size.height
                self.imageContainerBottomConstraint.constant = -self.commentContainer.frame.size.height - self.buttonsContainer.frame.size.height
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                fullScreenModeStatus = true
            } else {
                self.blur.isHidden = true
                self.iamgeContainerTopConstraint.constant = 0
                self.imageContainerBottomConstraint.constant = 0
                UIView.animate(withDuration: 0.3) {
                    self.view.layoutIfNeeded()
                }
                fullScreenModeStatus = false
            }
        }
        
    }
    @objc func swipeLeftAction () {
        imageGoesBack ()
    }
    
    @objc func swipeRightAction () {
        imageGoesForward()
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        imageGoesBack()
    }
    @IBAction func forwardButton(_ sender: UIButton) {
        imageGoesForward()
    }
    @IBAction func backButtonPressed(_ sender: UIButton) {
        imageArray[indexImg].like = likeButton.isSelected
        imageArray[indexImg].comment = commentSection.text
        UserDefaults.standard.set(encodable: imageArray, forKey: Keys.imageSet.rawValue)
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func likeButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
}

extension ViewerViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension ViewerViewController: UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        imageArray[indexImg].like = likeButton.isSelected
        imageArray[indexImg].comment = commentSection.text
        UserDefaults.standard.set(encodable: imageArray, forKey: Keys.imageSet.rawValue)
        return true
    }
}
