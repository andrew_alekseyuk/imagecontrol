import UIKit
import SwiftyKeychainKit
import MessageUI

class SetPasswordsViewController: UIViewController, MFMailComposeViewControllerDelegate {

    @IBOutlet weak var emailForm: UITextField!
    @IBOutlet weak var firstPassForm: UITextField!
    @IBOutlet weak var firstPassConfirmForm: UITextField!
    @IBOutlet weak var secondPassForm: UITextField!
    @IBOutlet weak var secondPassConfirmForm: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    
    var emailState = false
    var firstPasswordState = false
    var secondPasswordState = false
    
    let gradient = CAGradientLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
       
        
        registerButton.layer.cornerRadius = registerButton.frame.height / 2
        addGradient(button: registerButton)
        registerButton.clipsToBounds = true
        
        
    }
    
    func checkEmailForm () {
        if emailForm.text?.isEmpty == true {
            showAlert(title: "E-mail Issue", text: "Please enter your e-mail", myFunction: ())
        }
    }
    
    func checkFirstPassword () {
        
        if firstPassForm.text?.isEmpty == true || firstPassConfirmForm.text?.isEmpty == true {
            showAlert(title: "First Password Issue", text: "Please enter your first password", myFunction: ())
        } else if firstPassForm.text != firstPassConfirmForm.text {
            showAlert(title: "First Password Issue", text: "Passwords don't match", myFunction: ())
            firstPassForm.text = ""
            firstPassConfirmForm.text = ""
            //dosesn't work
            firstPassForm.becomeFirstResponder()
            //
        } else if firstPassForm.text == firstPassConfirmForm.text {
            do {
                try Manager.shared.keychain.set(firstPassForm.text!, for: Manager.shared.accessTokenKeyOne)
            } catch let error {
                debugPrint(error)
                
            }
            firstPasswordState = true
        }
            
        
    }
    func checkSecondPassword () {
        if secondPassForm.text?.isEmpty == true || secondPassConfirmForm.text?.isEmpty == true {
            showAlert(title: "Second Password Issue", text: "Please enter your second password", myFunction: ())
        } else if secondPassForm.text != secondPassConfirmForm.text {
            showAlert(title: "Second Password Issue", text: "Passwords don't match", myFunction: ())
            secondPassForm.text = ""
            secondPassConfirmForm.text = ""
            secondPassForm.becomeFirstResponder()
        } else if secondPassForm.text == secondPassConfirmForm.text {
            do {
                try Manager.shared.keychain.set(secondPassForm.text!, for: Manager.shared.accessTokenKeyTwo)
            } catch let error {
                debugPrint(error)
                
            }
            secondPasswordState = true
        }
            
        
    }
    
    func showSuccessMessage () {
        let alert = UIAlertController(title: "Registration success", message: "Press OK to go to login screen", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [self] action in
            let LoggingViewController = self.storyboard?.instantiateViewController(identifier: "LoggingViewController") as! LoggingViewController
            self.navigationController?.pushViewController(LoggingViewController, animated: true)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func sendEmail() {
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([emailForm.text!])
            mail.setSubject("ImageControl passwords")
            guard let firstPassword = firstPassForm.text else {return}
            guard let secondPassword = secondPassForm.text else {return}
            mail.setMessageBody("""
                <p>My ImageControl passwords are</p>
                <ul>
                <li>First password: <span style="background-color: #ccffcc;"><em>\(firstPassword)</em></span></li>
                <li>Second "double bottom" password: <span style="background-color: #ccffcc;"><em>\(secondPassword)</em></span></li>
                </ul>
                """, isHTML: true)

            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
            controller.dismiss(animated: true)
            showSuccessMessage()
        }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func addGradient(button: UIButton) {
                
 
                gradient.colors = [hexStringToUIColor(hex:"cdd806").cgColor,hexStringToUIColor(hex:"fc9d94").cgColor,hexStringToUIColor(hex:"cdd806").cgColor]
        
                gradient.opacity = 1
                gradient.startPoint = CGPoint(x: 0.0, y: 0.5)
                gradient.endPoint = CGPoint(x: 1.0, y: 0.5)
                
                gradient.frame = button.bounds
                gradient.name = "gradient"
                button.layer.insertSublayer(gradient, at: 0)
            }
    
    @IBAction func registerButtonPressed(_ sender: UIButton) {
        sender.pulsate()
        checkEmailForm()
        checkFirstPassword()
        checkSecondPassword()
        let alert = UIAlertController(title: "Do you want to send e-mail with passwords to yourself?", message: "Note that if you forget passwords you won't restore access to app", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
            self.sendEmail()
        }))
        alert.addAction(UIAlertAction(title: "No", style: .destructive, handler: { _ in
            self.showSuccessMessage()
            
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }
}

extension SetPasswordsViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailForm {
            print ("Enter pressed")
               textField.resignFirstResponder()
               firstPassForm.becomeFirstResponder()
            } else if textField == firstPassForm {
               textField.resignFirstResponder()
               firstPassConfirmForm.becomeFirstResponder()
            } else if textField == firstPassConfirmForm {
                textField.resignFirstResponder()
                secondPassForm.becomeFirstResponder()
            } else if textField == secondPassForm {
                textField.resignFirstResponder()
                secondPassConfirmForm.becomeFirstResponder()
            } else if textField == secondPassConfirmForm {
                textField.resignFirstResponder()
                
            }
        
           return true
          }
    
}


