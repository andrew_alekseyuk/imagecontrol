import UIKit

protocol FirstPasswordViewControllerDelegate: AnyObject {
    func getImageIndex (index: Int)
}

class FirstPasswordViewController: UIViewController, PlusCollectionViewCellDelegate, ImageCollectionViewCellDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var chosenImage = UIImageView()
    var SavedImagesArray:[Picture] = []
    var loadedImageArray:[Picture] = []
    
    weak var delegate: FirstPasswordViewControllerDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadImageArray()
        collectionView.reloadData()
        
        self.navigationController?.interactivePopGestureRecognizer!.delegate = self
    }
    
     func preferredScreenEdgesDeferringSystemGestures() -> UIRectEdge {
        return [.bottom]
    }
    
    
    func loadImageArray()  {
        guard let imageArray = UserDefaults.standard.value([Picture].self, forKey: Keys.imageSet.rawValue) else {return}
        self.SavedImagesArray = imageArray
    }
    
    
    func cellPressed (index: Int) {
        print(index)
        let ViewerViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewerViewController") as! ViewerViewController
        self.delegate?.getImageIndex(index: index)
        ViewerViewController.indexImg = index
        self.navigationController?.pushViewController(ViewerViewController, animated: true)
        
        
    }
    private func performImagePicker() {
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.modalPresentationStyle = .currentContext
        imagePicker.allowsEditing = false
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true, completion: nil)
    }
    private func performImagePickerFromCamera() {
        
        let imagePickerFromCamera = UIImagePickerController()
        imagePickerFromCamera.delegate = self
        imagePickerFromCamera.modalPresentationStyle = .currentContext
        imagePickerFromCamera.allowsEditing = false
        imagePickerFromCamera.sourceType = .camera
        
        present(imagePickerFromCamera, animated: true, completion: nil)
    }
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func viewerButtonPressed(_ sender: UIButton) {
        if UserDefaults.standard.value([Picture].self, forKey: Keys.imageSet.rawValue) == nil {
            let alert = UIAlertController(title: "Warning", message: "There are no images, please add images by pressing \"+\" button", preferredStyle: .alert)
            let okAction = UIAlertAction (title: "OK", style: .default, handler: nil)
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
        } else {
        
        let ViewerViewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewerViewController") as! ViewerViewController
        self.navigationController?.pushViewController(ViewerViewController, animated: true)
        }
    }
    
   func plusButtonPressed() {
        let alert = UIAlertController(title: "Choose source", message: "", preferredStyle: .actionSheet)
        let imagePicker = UIAlertAction(title: "Gallery", style: .default) { (_) in
            self.performImagePicker()
        }
        alert.addAction(imagePicker)
        let cameraPicker = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.performImagePickerFromCamera()
        }
        alert.addAction(cameraPicker)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
}

extension FirstPasswordViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[.originalImage] as? UIImage {
            
            let newImageURL = Manager.shared.saveImage(image: pickedImage)
            let newImage = Picture(imageURL: newImageURL, comment:"", like: false)
            if UserDefaults.standard.value([Picture].self, forKey: Keys.imageSet.rawValue) == nil {
                SavedImagesArray.append(newImage)
                picker.dismiss(animated: true, completion: nil)
                UserDefaults.standard.set(encodable: SavedImagesArray, forKey: Keys.imageSet.rawValue)
                collectionView.reloadData()
            } else {
                SavedImagesArray = UserDefaults.standard.value([Picture].self, forKey: Keys.imageSet.rawValue)!
                SavedImagesArray.append(newImage)
                picker.dismiss(animated: true, completion: nil)
                UserDefaults.standard.set(encodable: SavedImagesArray, forKey: Keys.imageSet.rawValue)
                SavedImagesArray = UserDefaults.standard.value([Picture].self, forKey: Keys.imageSet.rawValue)!
                collectionView.reloadData()
            }
        }
    }
}

extension FirstPasswordViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return SavedImagesArray.count + 1
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.item == 0 {
            
            guard let cellOne = collectionView.dequeueReusableCell(withReuseIdentifier: "PlusCollectionViewCell", for: indexPath) as? PlusCollectionViewCell else {
                return UICollectionViewCell ()
            }
            cellOne.delegate = self
            cellOne.configure()
            return cellOne
            
        } else {
            
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as? ImageCollectionViewCell else {
                return UICollectionViewCell ()
            }
            cell.delegate = self
            cell.configure(object: SavedImagesArray[indexPath.item - 1], index: indexPath.item)
            return cell 
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //let side: CGFloat = (view.frame.width) / 4 - 7
        let side: CGFloat = (view.frame.width) / 3 - 8
        
        return CGSize(width: side, height: side)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        
        return collectionView.contentInset
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
}

extension FirstPasswordViewController : UIGestureRecognizerDelegate {
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}





