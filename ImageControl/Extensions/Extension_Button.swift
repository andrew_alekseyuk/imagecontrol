import Foundation
import UIKit

extension UIButton {
    func pulsate () {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.3
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        pulse.autoreverses = false
        pulse.repeatCount = .infinity
        pulse.initialVelocity = 1
        pulse.damping = 1.0
        pulse.repeatCount = 1
        self.layer.add(pulse, forKey: nil)
    }
}
