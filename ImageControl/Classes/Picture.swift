import Foundation
import UIKit

enum Keys: String {
    case imageSet = "imageSet"
}

class Picture: Codable, Equatable {
    static func == (lhs: Picture, rhs: Picture) -> Bool {
        return lhs.imageURL == rhs.imageURL && lhs.imageURL == rhs.imageURL && lhs.like == rhs.like
    }
    
    //solution without UserDefaults//
    //var image: UIImage?
    var imageURL: String?
    var comment: String?
    var like: Bool?
    
    init(imageURL: String?, comment: String, like: Bool) {
        //init(imageURL: UIImage?, comment: String, like: Bool) {
        //self.image = image
        self.imageURL = imageURL
        self.comment = comment
        self.like = like
    }
    private enum CodingKeys: String, CodingKey {
        //case image
        case imageURL
        case comment
        case like
    }
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        //low productivity
        //guard let imageData = try container.decodeIfPresent(Data.self, forKey: .image) else {return}
        //             image = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(imageData) as? UIImage
        imageURL = try container.decodeIfPresent(String.self, forKey: .imageURL)
        comment = try container.decodeIfPresent(String.self, forKey: .comment)
        like = try container.decodeIfPresent(Bool.self, forKey: .like) 
        
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        //low productivity
        //let imageData = try NSKeyedArchiver.archivedData(withRootObject: image, requiringSecureCoding: false)
        try container.encode(self.imageURL, forKey: .imageURL)
        try container.encode(self.comment, forKey: .comment)
        try container.encode(self.like, forKey: .like)
    }
}
extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}


