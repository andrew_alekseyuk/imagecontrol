import UIKit

protocol PlusCollectionViewCellDelegate: AnyObject {
    func plusButtonPressed ()
}

class PlusCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var containerView: UIView!
    
    weak var delegate: PlusCollectionViewCellDelegate?
    
    func configure () {
        containerView.layer.borderWidth = 0.2
    }
    @IBAction func cellPlusPressed(_ sender: UIButton) {
        self.delegate?.plusButtonPressed()
    }
}
