import UIKit

protocol ImageCollectionViewCellDelegate: AnyObject {
    func cellPressed (index: Int)
}

class ImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var cellImage: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var pressRegisterView: UIView!
    var imageIndex = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
//        guard let imageArray = UserDefaults.standard.value([Picture].self, forKey: Keys.imageSet.rawValue) else {return}
//        self.imageArray = imageArray
    }
    weak var delegate: ImageCollectionViewCellDelegate?

    let tap = UITapGestureRecognizer (target: self, action: #selector(goToViewer))
    
    
    func configure (object: Picture, index: Int) {
        
        containerView.layer.borderWidth = 0.2
        
        guard let imageURL = object.imageURL else {return}
        let image = Manager.shared.loadImage(fileName: imageURL)
        cellImage.image = image
        cellImage.contentMode = .scaleAspectFill
        let tap = UITapGestureRecognizer (target: self, action: #selector(goToViewer))
        self.pressRegisterView.addGestureRecognizer(tap)
        imageIndex = index - 1
    }
    
    @objc func goToViewer() {
        self.delegate?.cellPressed(index: imageIndex)
    }
}
